$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "my_address/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "my_address"
  s.version     = MyAddress::VERSION
  s.authors     = ["Min Zeya Phyo"]
  s.email       = ["zeya@code2lab.com"]
  s.homepage    = "http://www.code2lab.com"
  s.summary     = "MYAdress is a single address engine to manage Geo-location address in Myanmar."
  s.description = "Myanmar Address is to provide a single engine to manage city, township, address lookup in Myanmar. It does provide a ISO country and state lookup for rest of the world"
  s.license     = "MIT"

  s.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.rdoc"]
  s.test_files = Dir["test/**/*"]

  s.add_dependency "rails", ">= 4.0.1"

  s.add_development_dependency "pg"
end
