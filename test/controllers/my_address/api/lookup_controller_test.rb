require 'test_helper'

module MyAddress
  class Api::LookupControllerTest < ActionController::TestCase
    test "should get country" do
      get :country
      assert_response :success
    end

    test "should get state" do
      get :state
      assert_response :success
    end

    test "should get city" do
      get :city
      assert_response :success
    end

    test "should get township" do
      get :township
      assert_response :success
    end

    test "should get street" do
      get :street
      assert_response :success
    end

  end
end
