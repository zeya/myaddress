require 'test_helper'

module MyAddress
  class CountriesControllerTest < ActionController::TestCase
    setup do
      @country = countries(:one)
    end

    test "should get index" do
      get :index
      assert_response :success
      assert_not_nil assigns(:countries)
    end

    test "should get new" do
      get :new
      assert_response :success
    end

    test "should create country" do
      assert_difference('Country.count') do
        post :create, country: { alt_spellings: @country.alt_spellings, calling_code: @country.calling_code, capital: @country.capital, cca2: @country.cca2, cca3: @country.cca3, ccn3: @country.ccn3, currency: @country.currency, locale_name: @country.locale_name, name: @country.name, region: @country.region, relevance: @country.relevance, subregion: @country.subregion, tld: @country.tld }
      end

      assert_redirected_to country_path(assigns(:country))
    end

    test "should show country" do
      get :show, id: @country
      assert_response :success
    end

    test "should get edit" do
      get :edit, id: @country
      assert_response :success
    end

    test "should update country" do
      patch :update, id: @country, country: { alt_spellings: @country.alt_spellings, calling_code: @country.calling_code, capital: @country.capital, cca2: @country.cca2, cca3: @country.cca3, ccn3: @country.ccn3, currency: @country.currency, locale_name: @country.locale_name, name: @country.name, region: @country.region, relevance: @country.relevance, subregion: @country.subregion, tld: @country.tld }
      assert_redirected_to country_path(assigns(:country))
    end

    test "should destroy country" do
      assert_difference('Country.count', -1) do
        delete :destroy, id: @country
      end

      assert_redirected_to countries_path
    end
  end
end
