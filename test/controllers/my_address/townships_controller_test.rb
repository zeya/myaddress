require 'test_helper'

module MyAddress
  class TownshipsControllerTest < ActionController::TestCase
    setup do
      @township = townships(:one)
    end

    test "should get index" do
      get :index
      assert_response :success
      assert_not_nil assigns(:townships)
    end

    test "should get new" do
      get :new
      assert_response :success
    end

    test "should create township" do
      assert_difference('Township.count') do
        post :create, township: { city_id: @township.city_id, code: @township.code, locale_name: @township.locale_name, name: @township.name }
      end

      assert_redirected_to township_path(assigns(:township))
    end

    test "should show township" do
      get :show, id: @township
      assert_response :success
    end

    test "should get edit" do
      get :edit, id: @township
      assert_response :success
    end

    test "should update township" do
      patch :update, id: @township, township: { city_id: @township.city_id, code: @township.code, locale_name: @township.locale_name, name: @township.name }
      assert_redirected_to township_path(assigns(:township))
    end

    test "should destroy township" do
      assert_difference('Township.count', -1) do
        delete :destroy, id: @township
      end

      assert_redirected_to townships_path
    end
  end
end
