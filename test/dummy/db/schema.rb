# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20140818084341) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "my_address_addresses", force: true do |t|
    t.string   "number"
    t.string   "building_name"
    t.string   "street_name"
    t.decimal  "latitude",      precision: 9, scale: 6
    t.decimal  "longtitude",    precision: 9, scale: 6
    t.integer  "street_id"
    t.integer  "township_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "my_address_addresses", ["street_id"], name: "index_my_address_addresses_on_street_id", using: :btree
  add_index "my_address_addresses", ["township_id"], name: "index_my_address_addresses_on_township_id", using: :btree

  create_table "my_address_cities", force: true do |t|
    t.string   "name"
    t.string   "locale_name"
    t.integer  "state_id"
    t.string   "code"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "my_address_cities", ["state_id"], name: "index_my_address_cities_on_state_id", using: :btree

  create_table "my_address_countries", force: true do |t|
    t.string   "name"
    t.string   "locale_name"
    t.string   "cca2"
    t.string   "ccn3"
    t.string   "cca3"
    t.string   "tld"
    t.string   "currency"
    t.string   "calling_code"
    t.string   "capital"
    t.string   "alt_spellings"
    t.string   "relevance"
    t.string   "region"
    t.string   "subregion"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "my_address_nationalities", force: true do |t|
    t.integer  "country_id"
    t.string   "name"
    t.string   "locale_name"
    t.string   "code"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "my_address_nationalities", ["country_id"], name: "index_my_address_nationalities_on_country_id", using: :btree

  create_table "my_address_states", force: true do |t|
    t.string   "name"
    t.string   "locale_name"
    t.integer  "country_id"
    t.string   "code"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "my_address_states", ["country_id"], name: "index_my_address_states_on_country_id", using: :btree

  create_table "my_address_streets", force: true do |t|
    t.string   "name"
    t.string   "locale_name"
    t.integer  "township_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "my_address_streets", ["township_id"], name: "index_my_address_streets_on_township_id", using: :btree

  create_table "my_address_townships", force: true do |t|
    t.string   "name"
    t.string   "locale_name"
    t.integer  "city_id"
    t.string   "code"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "my_address_townships", ["city_id"], name: "index_my_address_townships_on_city_id", using: :btree

end
