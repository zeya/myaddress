json.array!(@counties) do |country|
  json.extract! country, :id, :name, :locale_name, :cca2, :ccn3
end
