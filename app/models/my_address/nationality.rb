module MyAddress
  class Nationality < ActiveRecord::Base
    belongs_to :country
    
    def self.autocomplete_lookup(query, locale)
      if locale
        result = Nationality.select(:id, :name, :locale_name, :code).where("lower(locale_name) like lower('%#{query}%')").order("locale_name asc")
      
      else
        result = Nationality.select(:id, :name, :locale_name, :code).where("lower(name) like lower('%#{query}%')").order("name asc")
        
      end
    end
    
    def self.import_seed_data
      nationality = File.read("db/seeds/nationalities.csv")
      nationalities = nationality.split(",")
    
      Nationality.delete_all
    
      nationalities.each do |nation|
        n = Nationality.new
        n.name = nation
        n.locale_name = nation
        n.save
        
        puts "#{n.name} nationality added to db!"
      end
    
    end
  end
end
