module MyAddress
  class City < ActiveRecord::Base
    belongs_to :state
    has_many :townships
    
    def self.autocomplete_lookup(state_id, query, locale)
      if locale
        result = City.select(:id, :name, :locale_name, :code).where("lower(locale_name) like lower('%#{query}%') and state_id = #{state_id}").order("locale_name asc")
      
      else
        result = City.select(:id, :name, :locale_name, :code).where("lower(name) like lower('%#{query}%') and state_id = #{state_id}").order("name asc")
        
      end
    end
    
    def self.import_seed_data
       cities_json = JSON.parse(File.read("db/seeds/cities.json"));

       #Delete all previous records
       City.delete_all
     
       cities_json.each do |city|
         #puts state
         state = State.find_by_code(city["state_code"])
       
         c = City.new(:name => city["name"], :state => state, :locale_name => city["locale_name"], :code => city["code"])
         c.save
         
         puts "#{c.name} city added into db"
       end
    end
  end
end
