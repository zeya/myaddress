module MyAddress
  class Township < ActiveRecord::Base
    belongs_to :city
    has_many :streets
    
    def self.autocomplete_lookup(city_id, query, locale)
      if locale
        result = Township.select(:id, :name, :locale_name, :code).where("lower(locale_name) like lower('%#{query}%') and city_id = #{city_id}").order("locale_name asc")
      
      else
        result = Township.select(:id, :name, :locale_name, :code).where("lower(name) like lower('%#{query}%') and city_id = #{city_id}").order("name asc")
        
      end
    end
    
    def self.import_seed_data
       townships_json = JSON.parse(File.read("db/seeds/townships.json"));
     
     
       #Delete all previous records
       Township.delete_all
     
       townships_json.each do |township|
         #puts state
         city = City.find_by_code(township["city_code"])
       
         t = Township.new(:name => township["name"], :city => city, :locale_name => township["locale_name"], :code => township["code"])
         t.save
         
         puts "#{t.name} township added to db!"
       end
    end
  end
end
