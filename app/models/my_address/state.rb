module MyAddress
  class State < ActiveRecord::Base
    belongs_to :country
    has_many :cities
    
    def self.autocomplete_lookup(country_id, query, locale)
      if locale
        result = State.select(:id, :name, :locale_name, :code).where("lower(locale_name) like lower('%#{query}%') and country_id = #{country_id}").order("locale_name asc")
      
      else
        result = State.select(:id, :name, :locale_name, :code).where("lower(name) like lower('%#{query}%') and country_id = #{country_id}").order("name asc")
        
      end
    end
    
    def self.import_seed_data
       states_json = JSON.parse(File.read("db/seeds/states.json"));

       #Delete all previous records
       State.delete_all
     
       states_json.each do |state|
         #puts state
         country = Country.find_by_cca2(state["country_iso"])
       
         s = State.new(:name => state["name"], :country => country, :locale_name => state["locale_name"], :code => state["code"])
         s.save
         puts "#{s.name} state added into database"
       end
    end
    
  end
end
