module MyAddress
  class Street < ActiveRecord::Base
    belongs_to :township
    
    def self.autocomplete_lookup(township_id, query, locale)
      if locale
        result = Street.select(:id, :name).where("lower(locale_name) like lower('%#{query}%') and township_id = #{township_id}").order("locale_name asc")
      
      else
        result = Street.select(:id, :name).where("lower(name) like lower('%#{query}%') and township_id = #{township_id}").order("name asc")
        
      end
    end
    
    def self.import_seed_data
       streets_json = JSON.parse(File.read("db/seeds/streets.json"));
     
     
       #Delete all previous records
       Street.delete_all
     
       streets_json.each do |street|
         #puts state
         township = Township.find_by_code(street["township_code"])
       
         s = Street.new(:name => street["name"], :township_id => township.id , :locale_name => street["locale_name"])
         s.save
         
         puts "#{s.name} street name added to db!"
       end
    end
  end
end
