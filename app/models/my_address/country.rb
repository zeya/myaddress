module MyAddress
  class Country < ActiveRecord::Base
    
    def self.autocomplete_lookup(query, locale)
      if locale
        result = Country.select(:id, :name, :locale_name, :cca2).where("lower(locale_name) like lower('%#{query}%')").order("locale_name asc")
      
      else
        result = Country.select(:id, :name, :locale_name, :cca2).where("lower(name) like lower('%#{query}%')").order("name asc")
        
      end
    end
    
    def self.import_seed_data
       countries_json = JSON.parse(File.read("db/seeds/countries.json"));

       #Delete all previous records
       Country.delete_all
     
       countries_json.each do |country|
                 c = Country.new(country)
                 c.save
                 puts "#{c.name} country added to db."
       end
    end
  end
end
