module MyAddress
  class Address < ActiveRecord::Base
    belongs_to :street
    belongs_to :township
  end
end
