require_dependency "my_address/application_controller"

module MyAddress
  class TownshipsController < ApplicationController
    before_action :set_township, only: [:show, :edit, :update, :destroy]

    # GET /townships
    def index
      @townships = Township.all
    end

    # GET /townships/1
    def show
    end

    # GET /townships/new
    def new
      @township = Township.new
    end

    # GET /townships/1/edit
    def edit
    end

    # POST /townships
    def create
      @township = Township.new(township_params)

      if @township.save
        redirect_to @township, notice: 'Township was successfully created.'
      else
        render :new
      end
    end

    # PATCH/PUT /townships/1
    def update
      if @township.update(township_params)
        redirect_to @township, notice: 'Township was successfully updated.'
      else
        render :edit
      end
    end

    # DELETE /townships/1
    def destroy
      @township.destroy
      redirect_to townships_url, notice: 'Township was successfully destroyed.'
    end

    private
      # Use callbacks to share common setup or constraints between actions.
      def set_township
        @township = Township.find(params[:id])
      end

      # Only allow a trusted parameter "white list" through.
      def township_params
        params.require(:township).permit(:name, :locale_name, :city_id, :code)
      end
  end
end
