require_dependency "my_address/application_controller"

module MyAddress
  class Api::LookupController < ApplicationController
    def country
      @countries = Country.all.order("name asc")
    end

    def state
      @states = State.all.order("name asc")
    end

    def city
      @cities = City.all.order("name asc")
      
      # respond_to do |format|
 #        format.html
 #        format.json { render json: @cities}
 #      end
    end

    def township
      @townships = Township.all.order("name asc")
    end

    def street
      @streets = Street.all.order("name asc")
    end
  end
end
