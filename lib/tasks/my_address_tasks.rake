# desc "Explaining what the task does"
# task :my_address do
#   # Task goes here
# end

desc "Importing of Country/States/City/Township Seed data from lib/seed_data folder"
task :import_seed => :environment do
  MyAddress::Country.import_seed_data
  MyAddress::State.import_seed_data
  MyAddress::City.import_seed_data
  MyAddress::Township.import_seed_data
  MyAddress::Street.import_seed_data
  MyAddress::Nationality.import_seed_data  
end