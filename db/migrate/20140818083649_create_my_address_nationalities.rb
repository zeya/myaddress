class CreateMyAddressNationalities < ActiveRecord::Migration
  def change
    create_table :my_address_nationalities do |t|
      t.references :country, index: true
      t.string :name
      t.string :locale_name
      t.string :code

      t.timestamps
    end
  end
end
