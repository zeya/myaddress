class CreateMyAddressAddresses < ActiveRecord::Migration
  def change
    create_table :my_address_addresses do |t|
      t.string :number
      t.string :building_name
      t.string :street_name
      t.decimal :latitude, :precision => 9, :scale => 6
      t.decimal :longtitude, :precision => 9, :scale => 6
      t.references :street, index: true
      t.references :township, index: true

      t.timestamps
    end
  end
end
