class CreateMyAddressCities < ActiveRecord::Migration
  def change
    create_table :my_address_cities do |t|
      t.string :name
      t.string :locale_name
      t.references :state, index: true
      t.string :code

      t.timestamps
    end
  end
end
