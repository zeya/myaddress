class CreateMyAddressCountries < ActiveRecord::Migration
  def change
    create_table :my_address_countries do |t|
      t.string :name
      t.string :locale_name
      t.string :cca2
      t.string :ccn3
      t.string :cca3
      t.string :tld
      t.string :currency
      t.string :calling_code
      t.string :capital
      t.string :alt_spellings
      t.string :relevance
      t.string :region
      t.string :subregion

      t.timestamps
    end
  end
end
