class CreateMyAddressTownships < ActiveRecord::Migration
  def change
    create_table :my_address_townships do |t|
      t.string :name
      t.string :locale_name
      t.references :city, index: true
      t.string :code

      t.timestamps
    end
  end
end
