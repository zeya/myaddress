class CreateMyAddressStreets < ActiveRecord::Migration
  def change
    create_table :my_address_streets do |t|
      t.string :name
      t.string :locale_name
      t.references :township, index: true

      t.timestamps
    end
  end
end
