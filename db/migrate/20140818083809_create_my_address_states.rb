class CreateMyAddressStates < ActiveRecord::Migration
  def change
    create_table :my_address_states do |t|
      t.string :name
      t.string :locale_name
      t.references :country, index: true
      t.string :code

      t.timestamps
    end
  end
end
