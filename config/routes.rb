MyAddress::Engine.routes.draw do

  root "myhome#index"

  resources :addresses
  resources :streets
  resources :townships
  resources :cities
  resources :states
  resources :nationalities
  resources :countries
  
  namespace :api do
    get 'lookup/country'
    get 'lookup/state'
    get 'lookup/city'
    get 'lookup/township'
    get 'lookup/street'
  end
  
end
